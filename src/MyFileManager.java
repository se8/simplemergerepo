/*
    Load and save file
*/
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.FileWriter; 

public class MyFileManager {

    // singleton design pattern
    private static MyFileManager instance;

    private MyFileManager() {
        System.out.println("call MyFileManager constructor");   // test
    }

    public static MyFileManager getInstance() {
        if (instance == null)
            instance = new MyFileManager();
        return instance;
    }


    public File load() {
        File file = null;
        JFileChooser chooser = new JFileChooser();
        // contrain user to select only text file
        chooser.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
        chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        // chooser.setCurrentDirectory(new File("C:/test"));
        int returnVal = chooser.showOpenDialog(null);   // below code will not be executed until user action
        if (returnVal == JFileChooser.APPROVE_OPTION)   // user selected file
            file = chooser.getSelectedFile();      

        return file;
       
        /* 
            05/23(Wed)
            Changed signature of load() :
                What about just empty file and fail??? 
                At calling module, don't call JTextArea.setText when load file is null
            -> solution : 
        */
    }

    public boolean save(File file, String content) {
        if (file == null)
            return false;

        boolean flag = true;
        try {
            FileWriter fw = new FileWriter(file, false);
            fw.write(content);
            fw.close();
        } catch (Exception ex) {
            flag = false;
            ex.printStackTrace();
        }
        return flag;   
    }

    // just for testing  
    public static void main(String[] args) {
        MyFileManager mfm = MyFileManager.getInstance();

        // load test    
        File file = mfm.load();

        if (file == null) {
            System.out.println("File load FAIL");
        }   else {
            System.out.println("File load OKAY");
            System.out.println("loaded file : " + file.getName());
        }

        // save test
        String a = "hohoho\n333\n";
        if(mfm.save(file ,a))
            System.out.println("Save OKAY!");
        else System.out.println("Save FAIL!");

    }
}
