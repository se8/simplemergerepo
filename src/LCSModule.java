import java.util.ArrayList;
import java.util.List;

/*
    This contains utility comparing two files using
    LCS(Longest Common Subsequence) algorithm
    Ref) https://www.geeksforgeeks.org/printing-longest-common-subsequence/

*/
public class LCSModule {

    // singleton design pattern
    private static LCSModule instance;
    private LCSModule() {
        System.out.println("call LCSModule constructor");   // test
    }
    public static LCSModule getInstance() {
        if (instance == null)
            instance = new LCSModule();
        return instance;
    }


    public void logic(Model leftCompareM, Model rightCompareM) {

        String sL = leftCompareM.getFullContent(); //leftCompareM.getBeforeString();
        String sR = rightCompareM.getFullContent(); //rightCompareM.getBeforeString();
        ArrayList<HighlightStatus> highlightStatusListL = leftCompareM.getHighlightStatusList();
        ArrayList<HighlightStatus> highlightStatusListR = rightCompareM.getHighlightStatusList();
        highlightStatusListL.clear();
        highlightStatusListR.clear();

        String newLeftString = "";      // target
        String newRightString = "";     // target


        for (int i = 0; i < sL.length(); i++) {
            highlightStatusListL.add(HighlightStatus.CONFLICT);
        }
        for (int i = 0; i < sR.length(); i++) {
            highlightStatusListR.add(HighlightStatus.CONFLICT);
        }

        List<Integer> newLineListL = new ArrayList<>();
        List<Integer> newLineListR = new ArrayList<>();

        List<Integer> newLineMaxList = new ArrayList<>();


        // 1. get lcs string
        String lcsString = getLCSString(sL, sR, sL.length(), sR.length());

        // 2. compare s1 and lcsstring ; common -> false, else: true
        int idx = 0, lIdx = 0;
        while (idx < sL.length() || lIdx < lcsString.length() ) {
            if (lcsString.charAt(lIdx) == sL.charAt(idx)) {
                highlightStatusListL.set(idx, HighlightStatus.SAME);
                idx++; lIdx++;
            }
            else { idx++; }
        }

        // compare s2 and lcsstring ; common -> false, else: true
        idx =0; lIdx = 0;
        while (idx < sR.length() || lIdx < lcsString.length() ) {
            if (lcsString.charAt(lIdx) == sR.charAt(idx)) {
                highlightStatusListR.set(idx, HighlightStatus.SAME);
                idx++; lIdx++;
            }
            else { idx++; }
        }


        // 3. count newline between common char ~ next common char
        int j = 0;
        for(int i = 0 ; i < sL.length(); i++) {
            int num = 0;
            if(sL.charAt(i) == '\n' && highlightStatusListL.get(i)==HighlightStatus.SAME) {
                j = i+1;
                while(true) {
                    if (j == sL.length()) break;

                    else if(sL.charAt(j) =='\n') {
                        num++;
                    }
                    else if(highlightStatusListL.get(j) == HighlightStatus.SAME) {
                        break;
                    }
                    j++;
                }
                newLineListL.add(num);
            }
        }

        j = 0;
        for(int i = 0 ; i < sR.length(); i++) {
            int num = 0;
            if(sR.charAt(i) == '\n' && highlightStatusListR.get(i)==HighlightStatus.SAME) {
                j = i+1;
                while(true) {
                    if (j == sR.length()) break;

                    else if(sR.charAt(j) =='\n') {
                        num++;
                    }
                    else if(highlightStatusListR.get(j) == HighlightStatus.SAME) {
                        break;
                    }
                    j++;
                }
                newLineListR.add(num);
            }
        }

        // 4. create max btw newLineListL & newLineListR
        for (int i = 0 ; i < newLineListL.size(); i++) {
             newLineMaxList.add(Math.max(newLineListL.get(i), newLineListR.get(i)));
        }


        // set difference with max in newLineListL
        for (int i = 0 ; i < newLineListL.size() ; i++) {
            newLineListL.set(i, newLineMaxList.get(i) - newLineListL.get(i));
        }
        // set difference with max in newLineListR
        for (int i = 0 ; i < newLineListR.size() ; i++) {
            newLineListR.set(i, newLineMaxList.get(i) - newLineListR.get(i));
        }

        // 5. 스트링 안에서 현재문자 is (common && \n )
        // 1)   '\n' * <4. 에서 구한 개행문자 수>  추가할 index 구하기.

        ArrayList<Integer> indexToAddNewLineLeft = new ArrayList<>();
        ArrayList<String> concatStr1 = new ArrayList<>();

        for (int i = 0; i < sL.length(); i++ ) {
            if (sL.charAt(i) == '\n' && highlightStatusListL.get(i) == HighlightStatus.SAME) {
                indexToAddNewLineLeft.add(i+1); // '\n' 인 common char 뒤에 추가하고자, index 축적
            }
        }

        // 2) 기존 스트링을 잘라서 별도에 슬라이싱 보관
        concatStr1.add(sL.substring(0, indexToAddNewLineLeft.get(0)));
        for (int i = 0 ; i < newLineMaxList.size()-1; i++) {
            concatStr1.add(sL.substring(indexToAddNewLineLeft.get(i), indexToAddNewLineLeft.get(i+1)) );
        }
        concatStr1.add(sL.substring(indexToAddNewLineLeft.get(newLineListL.size()-1)));

        // 3) sL 스트링의 k번째(indexToAddNewLineLeft.get(i))에 newLineListL.get(i)만큼 \n 추가
        for (int i =0 ; i < indexToAddNewLineLeft.size() ; i++) {
            String temp = "";
            for (int z = 0 ; z < newLineListL.get(i); z++){
                temp += "\n";
                highlightStatusListL.add(indexToAddNewLineLeft.get(i), HighlightStatus.SAME);
            }

            	newLeftString = newLeftString + concatStr1.get(i) + temp;
        }

        // 1)   '\n' * <4. 에서 구한 개행문자 수>  추가할 index 구하기.

        ArrayList<Integer> indexToAddNewLineRight = new ArrayList<>();
        ArrayList<String> concatStr2 = new ArrayList<>();

        for (int i = 0; i < sR.length(); i++ ) {
            if (sR.charAt(i) == '\n' && highlightStatusListR.get(i) == HighlightStatus.SAME) {
                indexToAddNewLineRight.add(i+1); // '\n' 인 common char 뒤에 추가하고자, index 축적
            }
        }

        // 2) 기존 스트링을 잘라서 별도에 슬라이싱 보관
        concatStr2.add(sR.substring(0, indexToAddNewLineRight.get(0)));
        for (int i = 0 ; i < newLineMaxList.size()-1; i++) {
            concatStr2.add(sR.substring(indexToAddNewLineRight.get(i), indexToAddNewLineRight.get(i+1)) );
        }
        concatStr2.add(sR.substring(indexToAddNewLineRight.get(newLineListR.size()-1)));

        // 3) sL 스트링의 k번째(indexToAddNewLineLeft.get(i))에 newLineListL.get(i)만큼 \n 추가
        for (int i =0 ; i < indexToAddNewLineRight.size() ; i++) {
            String temp = "";
            for (int z = 0 ; z < newLineListR.get(i); z++){
                temp += "\n";
                highlightStatusListR.add(indexToAddNewLineRight.get(i), HighlightStatus.SAME);
            }
            newRightString = newRightString + concatStr2.get(i) + temp;
        }

        leftCompareM.setCompareResultString(newLeftString);
        rightCompareM.setCompareResultString(newRightString);

    }   //logic end


    // Returns length of LCS for X[0..m-1], Y[0..n-1]
    public String getLCSString(String X, String Y, int m, int n)
    {
        int[][] L = new int[m+1][n+1];

        for (int i=0; i<=m; i++)
        {
            for (int j=0; j<=n; j++)
            {
                if (i == 0 || j == 0)
                    L[i][j] = 0;
                else if (X.charAt(i-1) == Y.charAt(j-1))
                    L[i][j] = L[i-1][j-1] + 1;
                else
                    L[i][j] = Math.max(L[i-1][j], L[i][j-1]);
            }
        }
        // end of table creation

        int index = L[m][n];
        char[] lcsCharArr = new char[index];

        int i = m, j = n;
        while (i > 0 && j > 0)
        {
            if (X.charAt(i-1) == Y.charAt(j-1))
            {
                lcsCharArr[index-1] = X.charAt(i-1);
                i--;
                j--;
                index--;
            }
            // not same
            else if (L[i-1][j] > L[i][j-1])
                i--;
            else
                j--;
        }
        return String.valueOf(lcsCharArr);
    }

}
