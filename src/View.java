/*
    Show GUI panels
*/

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class View extends JFrame {

    // model
    private ArrayList<Model> modelList;
    // components
    private JPanel container;
    private JPanel middlePanel;
    private JButton compareBtn, copyLeftBtn, copyRightBtn;
    // components - panel
    private ArrayList<EditPanelView> editPanelList;
    // constructor
    public View(ArrayList<Model> modelList) {
        // set model
        this.modelList = modelList;

        // create container for layout
        container = new JPanel();
        container.setLayout(new BoxLayout(container, BoxLayout.X_AXIS));
        container.setAlignmentX(Component.CENTER_ALIGNMENT);

        // create middle panel
        middlePanel = new JPanel();
        middlePanel.setLayout(new BoxLayout(middlePanel, BoxLayout.Y_AXIS));

        compareBtn = new JButton("Compare");
        copyLeftBtn = new JButton("CopyToLeft (<-)");
        copyRightBtn = new JButton("CopyToRight (->)");
        compareBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        copyLeftBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        copyRightBtn.setAlignmentX(Component.CENTER_ALIGNMENT);

        // add elements to middle panel
        middlePanel.add(compareBtn);
        middlePanel.add(copyLeftBtn);
        middlePanel.add(copyRightBtn);

        // instantiate two edit panels
        editPanelList = new ArrayList<>();
        editPanelList.add(new EditPanelView("leftPanel", modelList.get(0)));
        editPanelList.add(new EditPanelView("rightPanel", modelList.get(1)));

        // add components to container
        container.add(editPanelList.get(0));
        container.add(middlePanel);
        container.add(editPanelList.get(1));

        // mainView settings
        this.add(container);    // add container
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(800, 600);

    }

    public ArrayList<EditPanelView> getEditPanelList() {
        return this.editPanelList;
    }

    public void showSaveDialog(boolean success) {
        String msg;
        if (success) {
            msg = "Save success!";
        } else {
            msg = "Save failed! Please try again";
        }
        JOptionPane.showMessageDialog(this, msg);
    }

    public void addCompareListener(ActionListener al) {
        compareBtn.addActionListener(al);
    }

    public void addCopyToLeftListener(ActionListener al) {
        copyLeftBtn.addActionListener(al);
    }

    public void addCopyToRightListener(ActionListener al) {
        copyRightBtn.addActionListener(al);
    }

}
