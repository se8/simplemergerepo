import java.util.ArrayList;

/*
    Main program runs by this class
*/
public class Main {

    public static void main(String[] args) {
        ArrayList<Model> modelList = new ArrayList<Model>();
        modelList.add(new Model());
        modelList.add(new Model());

        View view = new View(modelList);

        Controller controller = new Controller(view, modelList);

        view.setVisible(true);

    }
}
