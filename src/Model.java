/*
*   Created at 18.05.27
*   This class defines content model on panel
* */

import java.io.File;
import java.util.ArrayList;

enum HighlightStatus {
    CONFLICT, NEW_LINE, FAKE_LINE, SAME;
}

// inner class
public class Model {
    private ArrayList<HighlightStatus> highlightStatusList = new ArrayList<>();
    private String fullContent;
    private File file;
    private String compareResultString;

    // getter method
    public String getFullContent() {
        return this.fullContent;
    }

    // setter(set or update) method
    public void setFullContent(String newContent) {
        this.fullContent = newContent;
    }

    public File getFile() {
        return this.file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public ArrayList<HighlightStatus> getHighlightStatusList() {
        return highlightStatusList;
    }
    public String getCompareResultString() {
        return compareResultString;
    }

    public void setCompareResultString(String compareResultString) {
        this.compareResultString = compareResultString;
    }

}



