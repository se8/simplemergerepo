import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.stream.Stream;

/*
    Controller
 */
public class Controller {

    // model
    private ArrayList<Model> modelList;
    // view
    private View view;
    // view - helper to access edit panels
    private ArrayList<EditPanelView> editPanelList;
    // LCS logic module
    private LCSModule lcsModule;
    // file manager
    private MyFileManager myFileManager;

    // highlighters
    private Highlighter.HighlightPainter conflictLinePainter;       // pastel pink

    // constructor
    public Controller(View view, ArrayList<Model> modelList) {

        // set model and view
        this.modelList = modelList;
        this.view = view;

        // set helper list to access edit panels
        this.editPanelList = view.getEditPanelList();

        // get file manager
        myFileManager = MyFileManager.getInstance();
        // get lcs module
        lcsModule = LCSModule.getInstance();

        // add listeners
        for (EditPanelView panel : editPanelList) {
            panel.addLoadListener(new LoadListener(panel));      // load listener
            panel.addEditListener(new EditListener(panel));      // edit listener
            panel.addSaveListener(new SaveListener(panel));      // save listener
        }

        view.addCompareListener(new CompareListener());     // compare listener
        view.addCopyToLeftListener(new CopyToLeftListener());   // copy left listener
        view.addCopyToRightListener(new CopyToRightListener()); // copy right listener

        // set painter for highlight
        conflictLinePainter = new DefaultHighlighter.DefaultHighlightPainter(new Color(255, 238, 245));
    }

    // listener for load button ; in panel
    class LoadListener implements ActionListener {
        private EditPanelView panelView;

        public LoadListener(EditPanelView panelView) {
            this.panelView = panelView;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            // load file
            File file = myFileManager.load();
            if (file == null)   // if user canceled to load or failed to load
                return;

            removeCompareResults();

            panelView.getModel().setFile(file);

            StringBuilder contentBuilder = new StringBuilder();

            try (Stream<String> stream = Files.lines( file.toPath(),
                    StandardCharsets.UTF_8)) {
                stream.forEach(s -> contentBuilder.append(s).append("\n"));
            }
            catch (IOException ex) {
                ex.printStackTrace();
                return;
            }
            String content = contentBuilder.toString();
            // set text of view
            panelView.setText(content);
            // set text of model
            panelView.getModel().setFullContent(content);
            // enable save and edit
            panelView.disableEdit();    // press edit button if user want to edit
            panelView.enableEditSaveBtn();

        }
    }

    // listener for edit button ; in panel
    class EditListener implements ActionListener {
        private EditPanelView panelView;

        public EditListener(EditPanelView panelView) {
            this.panelView = panelView;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            removeCompareResults();

            // restore text
            for (EditPanelView pv: editPanelList) {
                pv.setText(pv.getModel().getFullContent());
            }

            panelView.enableEdit();
        }
    }

    // listener for save button ; in panel
    class SaveListener implements ActionListener {
        private EditPanelView panelView;

        public SaveListener(EditPanelView panelView) {
            this.panelView = panelView;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            panelView.getModel().setFullContent(panelView.getText());
            if(myFileManager.save(panelView.getModel().getFile(), panelView.getText())) {
                // success
                view.showSaveDialog(true);
            } else {    // failure
                view.showSaveDialog(false);
            }
        }
    }

    // listener for compare button
    class CompareListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // should work when two files are loaded
            for (Model m: modelList) {
                if (m.getFile()==null) return;
            }

            for (EditPanelView panel : editPanelList) {
                if (!panel.isHighlightRemoved()) return;
            }

            // set content to model
            for (int i =0 ; i < modelList.size() ; i++)
                modelList.get(i).setFullContent( editPanelList.get(i).getText());

            removeCompareResults();

            /* convey leftModel, rightModel to LCSModule */
            //CompareComponent m1 = new CompareComponent(modelList.get(0).getFullContent());
            //CompareComponent m2 = new CompareComponent(modelList.get(1).getFullContent());

            lcsModule.logic(modelList.get(0), modelList.get(1));

            /* put compare result content to panel */
            for (EditPanelView panel : editPanelList)
                panel.setText(panel.getModel().getCompareResultString());

            /* highlight based on textarea */
            highlight();

            for (EditPanelView pv: editPanelList)
                pv.disableEdit();

        }
    }

    // listener for copyToLeft button
    class CopyToLeftListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // should work when two files are loaded
            for (Model m: modelList) {
                if (m.getFile()==null) return;
            }
            // TODO:
        }
    }

    // listener for copyToLeft button
    class CopyToRightListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // should work when two files are loaded
            for (Model m: modelList) {
                if (m.getFile()==null) return;
            }
            // TODO :
        }
    }

    /* put result on the each panel     */
    public void highlight() {

        for (EditPanelView panel : editPanelList) {
            DefaultHighlighter highlighter = (DefaultHighlighter) panel.getHighLighter();
            highlighter.setDrawsLayeredHighlights(false);
        }

        for (EditPanelView panel : editPanelList) {
            Model model = panel.getModel();
            ArrayList<HighlightStatus> highlightStatusList = model.getHighlightStatusList();

            for (int i = 0 ; i < highlightStatusList.size() ; i++ ) {
                HighlightStatus hStatus = highlightStatusList.get(i);

                switch(hStatus) {

                    case CONFLICT:
                        try {
                            panel.getHighLighter().addHighlight(i, i+1, conflictLinePainter);
                        } catch (BadLocationException e) {
                            e.printStackTrace();
                        }
                        break;

                    default :
                        break;
                }
            }
        }

    }

    public void removeCompareResults() {
        // remove highlights
        for (EditPanelView v: editPanelList)
            v.removeHighLights();
    }

}
