import javax.swing.*;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.awt.event.ActionListener;

/*
    Two edit panels will be instantiated (side-by-side) within a main window
    On top of each edit panel, there are buttons labeled with "Load", "Edit", "Save"
*/
public class EditPanelView extends JPanel {

    private Model simpleMergeModel;

    private JPanel subContainer;
    private JLabel label;
    private JButton loadBtn, editBtn, saveBtn;
    private JScrollPane scrollTextArea;
    private JTextArea textArea;
    private Highlighter highlighter;

    public EditPanelView(String labelName, Model model) {
        // set simpleMergeModel
        this.simpleMergeModel = model;

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBackground(new Color(135, 206, 235));    // skyblue color

        subContainer = new JPanel();
        subContainer.setLayout(new BoxLayout(subContainer, BoxLayout.X_AXIS));
        subContainer.setBackground(new Color(204, 255, 203));   // mint color

        // create label, buttons, text
        label = new JLabel(labelName);

        loadBtn = new JButton("Load");
        editBtn = new JButton("Edit");
        saveBtn = new JButton("Save");
        editBtn.setEnabled(false);      // diable edit, save as default
        saveBtn.setEnabled(false);

        textArea = new JTextArea();
        textArea.setBackground(Color.WHITE);
        textArea.setEditable(false);    // disable edit as default
        scrollTextArea = new JScrollPane(textArea);

        // add buttons to subcontainer
        subContainer.add(loadBtn);
        subContainer.add(editBtn);
        subContainer.add(saveBtn);

        // add to maincontainer
        this.add(label);
        this.add(subContainer);
        this.add(scrollTextArea);
        
        // set highlighter
        this.highlighter = this.textArea.getHighlighter();
    }
    // getter
    public Model getModel() { return this.simpleMergeModel; }
    public String getText() {
        return textArea.getText();
    }
    // setter
    public void setText(String str) {
        this.textArea.setText(str);
    }

    public void enableEditSaveBtn() {
        this.saveBtn.setEnabled(true);
        this.editBtn.setEnabled(true);
    }

    public void enableEdit() {
        if (this.simpleMergeModel.getFile() != null)
            this.textArea.setEditable(true);
    }

    public void disableEdit() {
        this.textArea.setEditable(false);
    }

    public void addLoadListener(ActionListener al) {
        loadBtn.addActionListener(al);
    }
    public void addEditListener(ActionListener al) {
        editBtn.addActionListener(al);
    }
    public void addSaveListener(ActionListener al) {
        saveBtn.addActionListener(al);
    }

    public Highlighter getHighLighter() {
        return this.textArea.getHighlighter();
    }

    public void removeHighLights() {
        getHighLighter().removeAllHighlights();
    }

    public boolean isHighlightRemoved() {
        return this.textArea.getHighlighter().getHighlights().length == 0;
    }
}
