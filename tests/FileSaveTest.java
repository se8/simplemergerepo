import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/* Test for File Save

    target : MyFileManager
    sub-target : Model

    */

public class FileSaveTest {
    private MyFileManager myFileManager;

    @Before
    public void initialize() throws Exception {    // equal to "setUp()"
        System.out.println("Before testing...");
        myFileManager = MyFileManager.getInstance();
    }

    @Test
    public void testSave() throws Exception {    // test method (in fact, method name doesn't matter)
        System.out.println("Performing test...");

        String testMsg = "This is test message called from save test in SimpleMerge Project.";

        Model model = new Model();
        model.setFile(new File("testdata/sample.txt"));

        // test file was set correct
        assertTrue(model.getFile().isFile());

        long beforeModified = model.getFile().lastModified();

        // test if save was performed
        assertTrue(myFileManager.save(model.getFile(), testMsg));

        // test if file was modified
        assertNotEquals(beforeModified, model.getFile().lastModified());

    }

    @After
    public void disposeObjects() throws Exception { // equal to "tearDown()"
        System.out.println("After testing...");
    }



}